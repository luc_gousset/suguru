#ifndef __SUGURU_H__
#define __SUGURU_H__

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <unistd.h>

#define EMPTY 0
#define TRUE 1
#define FALSE 0

//set la couleur d'arrière-plan dans le terminale
#define couleur(param) printf("\033[%sm",param)
//efface le texte du terminale
#define clear() printf("\033[H\033[J")

//Renvoie min ou max entre X, Y
#define MAX(x, y) (((x) > (y)) ? (x) : (y))
#define MIN(x, y) (((x) < (y)) ? (x) : (y))


typedef struct Case Case;

typedef struct Area 
{
    Case** area_list; //Liste des références des cases de la zone
    int8_t nb_case;
    int8_t area_id;

}Area;

struct Case
{
    int8_t value; //Valeur de la case
    Area* area; //Référence de la zone
    int8_t i; //Coordonné i
    int8_t j; //Coordonné j
    int8_t is_alterable; //Case modifiable ?
    //liste des possibilités
    //ex: possibility[0]=FALSE, la valeur 1 est un coup possible
    //possibility[1]=TRUE, la valeur 2 n'est pas un coup possible
    int8_t * possibility; 
};

typedef struct Damier
{
    Case** grid; //Matrice de point
    Area* area; //Liste des zones
    int8_t line; //nombre de ligne
    int8_t column; //nombre de colonne
    int8_t area_nb;//nb de zone
    int8_t nb_case_rempli;//nombre de case rempli dans la grille
    int8_t max_area_size; //taille max d'une zone
    
}Damier;


#endif