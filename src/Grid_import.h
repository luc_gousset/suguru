#ifndef __GRID_IMPORT_H__
#define __GRID_IMPORT_H__

#include "Suguru.h"

#define Grille_i 2
#define Grille_j 2
#define Area_nb 2

extern int8_t grille_default[Grille_i][Grille_j][2];

void importGridFromArray_raw(Damier* damier, int8_t array[Grille_i][Grille_j][2]);


#endif