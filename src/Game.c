#include "Game.h"
#include "Solver.h"

//alloue l'espace mémoire pour un tableau 2d de cases de taille n*m
Case ** create2dArray(int n, int m)
{
    Case ** res = malloc(n*sizeof(Case*)); 
    for(int i = 0; i<n; i++)
    {
        res[i] = malloc(m*sizeof(Case));
    }
    return res;
}


/*
Renvoie une matrice de case 3*3 des voisin directe de la case x,y
La case x,y est au centre de la liste
(-1,-1) si le voisin est un mur
*/

Case ** getNeighboor(Damier *damier, int x, int y)
{
    int xmax = damier->line;
    int ymax = damier->column;
    Case ** res = create2dArray(3,3);
    
    for (int i = -1;i<2;i++)
    {
        for(int j=-1; j<2;j++)
        {
            res[i+1][j+1].area = NULL;
            //On verifie si on est dans un "mur"
            if(x+i==-1 || x+i==xmax || y+j==-1 || y+j==ymax)
            {
                res[i+1][j+1].value = -1;
                res[i+1][j+1].i = i+1;
                res[i+1][j+1].j = j+1;
            }else
            {
                res[i+1][j+1] = damier->grid[i+x][j+y];
                res[i+1][j+1] = damier->grid[i+x][j+y];
                res[i+1][j+1] = damier->grid[i+x][j+y];
            }
        }
    }

    return res;
}



//Determine si un coup est légal
//check si chiffre a faire 
uint8_t isPlayLegit(Damier * damier, int x, int y, int newValue)
{
    //Check si la case est modifiable
    if(!damier->grid[x][y].is_alterable)
        return FALSE;

    //test si depassement
    if(x<0 || x>damier->line || y<0 || y>damier->column)
        return FALSE;

    //Test des voisins
    Case ** neighboor = getNeighboor(damier, x, y);
    for (int i = 0; i<3;i++)
    {
        for (int j = 0; j<3; j++)
        {
            //Si un voisin à la même valeur
            if((i!=1 || j!=1) && neighboor[i][j].value==newValue)
            {
                free2dArray(neighboor, 3);
                return FALSE;
            }    
        }
    }

    //Test de la zone
    int8_t nb_case = damier->grid[x][y].area->nb_case;
    for(int i = 0; i<nb_case;i++)
    {
        if(damier->grid[x][y].area->area_list[i]->value==newValue)
        {
            free2dArray(neighboor, 3);
            return FALSE;
        }
            
    }
    free2dArray(neighboor, 3);
    return(TRUE);
}


//Initialise la structure Damier
void initSuguru(Damier *damier, int8_t column, int8_t line, int8_t area_nb)
{
    damier->column = column;
    damier->line = line;
    damier->grid = create2dArray(line, column);
    damier->area_nb = area_nb;   
    damier->nb_case_rempli = 0;
    damier->max_area_size = 0;
}

//Print la grille
void printGrid(Case **grid, int column, int line)
{   
    printf("Grille:\n");
    char* list_couleur[8] = {"40","41","42","43","44","45","46","47"}; 
    for (int i = 0; i<line; i++)
    {
        for(int y = 0; y<column; y++)
        {
            if(grid[i][y].area!=NULL)
            {
                //set la couleur en fonction de la zone
                couleur(list_couleur[grid[i][y].area->area_id]);
            }
            else
            {
                couleur("0");
            }
            //print de la valeur
            if (grid[i][y].value == 0)
                printf("     ");
            else
                printf("  %d  ", grid[i][y].value);

            if (y==column-1)
                printf("\n");
        }
    }
    couleur("0");
}

void printPossibility(Damier * damier)
{   
    printf("Possibilité:\n");
    char* list_couleur[8] = {"40","41","42","43","44","45","46","47"}; 
    for (int i = 0; i<damier->line; i++)
    {
        for(int y = 0; y<damier->column; y++)
        {
            if(damier->grid[i][y].area!=NULL)
            {
                //set la couleur en fonction de la zone
                couleur(list_couleur[damier->grid[i][y].area->area_id]);
            }
            else
            {
                couleur("0");
            }
            //affichage des possibilités de la case
            for(int u = 0; u<damier->max_area_size; ++u)
            {
                if(damier->grid[i][y].possibility[u] && u<damier->grid[i][y].area->nb_case)
                    printf("%d", u+1);
                else
                    printf(" ");
            }
            
            if (y==damier->column-1)
                printf("\n");
        }
    }
    couleur("0");
}

//libère l'espace mémoire d'une matrice de case
void free2dArray(Case **array, int x)
{
    for(int i = 0; i<x; i++)
    {
        free(array[i]);
    }
    free(array);
}

//Joue un coup en fonction des inputs utilisateurs
// Renvoie 1 si le coup est valide
//0 sinon
int jouer_coup(Damier* damier)
{
    char x[2];
    char y[2];
    char value[2];
    printf("Entrer numero de ligne (1 à n)\n");
    scanf("%s", x);
    fflush(stdout);
    printf("Entrer numero de colone (1 à m)\n");
    scanf("%s", y);
    fflush(stdout);
    printf("Entrer valeur\n");
    scanf("%s", value);
    fflush(stdout);
    if (isPlayLegit(damier, atoi(x)-1, atoi(y)-1, atoi(value)))
    {
        printf("Coup valide\n");
        fflush(stdout);
        damier->grid[atoi(x)-1][atoi(y)-1].value = atoi(value);
        damier->nb_case_rempli++;
        sleep(1);
        return 1;
    }else
    {
        printf("Coup invalide\n");
        fflush(stdout);
        sleep(1);
        return 0;
    }
    clear();
}

void start_game(char* choice)
{ 
    printf("  .--.--.                  ,----..                 ,-.----.                \n");
    printf(" /  /    '.          ,--, /   /   \\           ,--, \\    /  \\           ,--,  \n");
    printf("|  :  /`. /        ,'_ /||   :     :        ,'_ /| ;   :    \\        ,'_ /|  \n");                                                                                
    printf(";  |  |--`    .--. |  | :.   |  ;. /   .--. |  | : |   | .\\ :   .--. |  | : \n");   
    printf("|  :  ;_    ,'_ /| :  . |.   ; /--`  ,'_ /| :  . | .   : |: | ,'_ /| :  . |   \n");
    printf(" \\  \\    `. |  ' | |  . .;   | ;  __ |  ' | |  . . |   |  \\ : |  ' | |  . .    \n"),
    printf("  `----.   \\|  | ' |  | ||   : |.' .'|  | ' |  | | |   : .  / |  | ' |  | |  \n");
    printf("  __ \\  \\  |:  | | :  ' ;.   | '_.' ::  | | :  ' ; ;   | |  \\ :  | | :  ' ;   \n");
    printf(" /  /`--'  /|  ; ' |  | ''   ; : \\  ||  ; ' |  | ' |   | ;\\  \\|  ; ' |  | '  \n");
    printf("'--'.     / :  | : ;  ; |'   | '/  .':  | : ;  ; | :   ' | \\.':  | : ;  ; |         \n");
    printf("'--'.     / :  | : ;  ; |'   | '/  .':  | : ;  ; | :   ' | \\.':  | : ;  ; |         \n");
    printf("  `--'---'  '  :  `--'   \\   :    /  '  :  `--'   \\:   : :-'  '  :  `--'   \\        \n");
    printf("            :  ,      .-./\\   \\ .'   :  ,      .-./|   |.'    :  ,      .-./        \n");
    printf("             `--`----'     `---`      `--`----'    `---'       `--`----'   \n");
    printf("\n");
    printf("1-Lancer grille default\n");
    printf("\nEntrer votre choix\n");


    fgets(choice, 5, stdin);

}

//Main loop du jeu
void suguru_loop()
{
    //Init des variables
    int quit = FALSE;
    int start = FALSE;
    int game_on = FALSE;
    int show_possibility = FALSE;
    char *choice = malloc(5*sizeof(char));
    char *game_choice = malloc(20*sizeof(char));
    Damier *damier = NULL; 
    char* solve_status = NULL;

    while (!quit)
    {   
        if (damier!=NULL)
            free(damier);
        damier = malloc(sizeof(Damier));
        while(!start)
        {
            start_game(choice);
            //Si l'utilisateur veut lancer le jeu sur la grille défault
            if (strcmp(choice, "1\n")==0)
            {
                initSuguru(damier, Grille_j, Grille_i, Area_nb);
                importGridFromArray_raw(damier, grille_default);
                start = TRUE;
                game_on = TRUE;
                clear();
            }
            else
            {
                clear();
                start = FALSE;
            }
        }

        while (game_on)
        {
            //si texte d'aide n'est pas NULL
            if(solve_status!=NULL)
            {
                printf("%s\n", solve_status);
                free(solve_status);
                solve_status = NULL;
            }
            //Affichage de la grille    
            printGrid(damier->grid, damier->column, damier->line);
            printf("\n");
            reduce_possibility_full(damier);

            //Affichage des possibilités
            if(show_possibility)
                printPossibility(damier);
            //Proposition des choix
            printf("\n");
            printf("1-Jouer un coup\n");
            printf("2-Aide\n");
            printf("3-Afficher/cacher possibilités\n");

            fgets(game_choice, 5, stdin);
            //Lance la fonction pour jouer un coup
            if (strcmp(game_choice, "1\n")==0)
            {
                jouer_coup(damier);
                reduce_possibility_full(damier);
            }
            //Lance l'aide
            else if (strcmp(game_choice, "2\n")==0)
            {
                reduce_possibility_full(damier);
                solve_status = solve(damier);
            }
            //Authorise l'affichage des possibilités
            else if (strcmp(game_choice, "3\n")==0)
            {
                if (show_possibility)
                    show_possibility = FALSE;
                else
                    show_possibility = TRUE;
            }

            //si la grille est remplis (on a gagné)
            if(damier->nb_case_rempli == damier->column*damier->line)
            {
                clear();
                printGrid(damier->grid, damier->column, damier->line);
                printf("Gagné\n");
                fflush(stdout);
                sleep(2);
                game_on = FALSE;
                start = FALSE; 
            }

            clear();

        }
    }
}