#include "Solver.h"
#include "Game.h"

void reduce_possibility_full(Damier* damier)
{
    for(int i = 0; i<damier->line; ++i)
    {
        for(int j = 0; j<damier->column; ++j)
        {
            reduce_possibility_case(damier, i, j);
        }
    }
}

void reduce_possibility_case(Damier* damier, int i, int j)
{
    for(int y = 0; y<damier->grid[i][j].area->nb_case; ++y)
    {
        if(isPlayLegit(damier, i, j, y+1)==FALSE || damier->grid[i][j].value)
            damier->grid[i][j].possibility[y]=0;

    }
}

uint8_t isPossibilityUnique_case(Damier* damier, int i, int j)
{
    Case c = damier->grid[i][j];

    int multiple_possibility = FALSE;
    int value = 0;

    for(int y = 0; y<damier->grid[i][j].area->nb_case; ++y)
    {
        if(c.possibility[y] == 1)
        {
            if(multiple_possibility==FALSE)
            {
                multiple_possibility = TRUE;
                value = y+1;
            } 
            else
                return FALSE;
        }
        
    }
    if(multiple_possibility)
        return value;
    else
        return FALSE;
}

uint8_t isPossibilityUnique(int8_t* array, int nb_case)
{
    int multiple_possibility = FALSE;
    int value = 0;

    for(int y = 0; y<nb_case; ++y)
    {
        if(array[y] == 1)
        {
            if(multiple_possibility==FALSE)
            {
                multiple_possibility = TRUE;
                value = y+1;
            } 
            else
                return FALSE;
        }
        
    }
    if(multiple_possibility)
        return value;
    else
        return FALSE;
}

int8_t* common_between_two_array(int8_t* arr1, int8_t* arr2, int size)
{
    int8_t* res = malloc(sizeof(int8_t)*size);
    memset(res, 0, sizeof(int8_t)*size);

    for(int i = 0; i<size; ++i)
    {
        if(arr1[i]==arr2[i])
            res[i]=arr1[i];
    }
    return res;
}


//Technique Simple

uint8_t fullBlock(Damier* damier, int i, int j)
{
    Case *c = &damier->grid[i][j];
    
    
    int value = isPossibilityUnique_case(damier, i, j);
    if(!value)
        return FALSE;
    for(int y = 0; y<c->area->nb_case; ++y)
    {
        if((c->area->area_list[y]->i!=i || c->area->area_list[y]->j!=j) && c->area->area_list[y]->value==0)
        {
            return FALSE;
        }
    }
    return value;

}

uint8_t unique(Damier* damier, int i, int j)
{
    Case *c = &damier->grid[i][j];
    int8_t* possibilities = malloc(sizeof(int8_t)*c->area->nb_case);
    memcpy(possibilities, c->possibility, sizeof(int8_t)*c->area->nb_case);
    
    Area area = *(c->area);
    
    for(int k = 0; k<area.nb_case; ++k)
    {
        if(c->area->area_list[k]->i!=i || c->area->area_list[k]->j!=j)
            continue;
        if(area.area_list[k]->value!=0)
        {
            possibilities[area.area_list[k]->value-1]=0;
        }else
        {
            for(int y = k+1; y<area.nb_case; ++y)
            {
                for(int possi = 0; possi<area.nb_case; possi++)
                {
                    if(area.area_list[k]->possibility[k]==area.area_list[y]->possibility[y])
                        continue;
                    possibilities[possi]=0;
                }
                
                
            }
        }
        
    }
    free(possibilities);
    return isPossibilityUnique(possibilities, area.nb_case);
}

//technique intermédiaire
uint8_t simpleRound(Damier* damier, int i, int j)
{
    Case *c = &damier->grid[i][j];
    int8_t* common_element = NULL;
    Case** neighbhoor = getNeighboor(damier, i, j);
    int min_area = 0;
    Case* t;
    for (int x = 0; x<3; ++x)
    {
        for(int y = 0; y<3; ++y)
        {
            if(neighbhoor[x][y].value==0 && neighbhoor[x][y].area->area_id!=c->area->area_id)
            {
                min_area = MIN(c->area->nb_case, neighbhoor[x][y].area->nb_case);
                common_element = common_between_two_array(c->possibility, neighbhoor[x][y].possibility, min_area);
                for(int poss = 0; poss<min_area; ++poss)
                {
                    if(common_element[poss])
                    {
                        for(int neigh_area = 0; neigh_area<neighbhoor[x][y].area->nb_case; ++neigh_area)
                        {
                            t = neighbhoor[x][y].area->area_list[neigh_area];
                            if(t->possibility[poss]==1 && (abs(t->i-c->i)>=2 || abs(t->j-c->j)>=2))
                                break;
                            if(neigh_area==neighbhoor[x][y].area->nb_case-1)
                            {
                                c->possibility[poss]=0;
                                return poss+1;
                            }
                                
                        }
                    }
                }
            }
        }
    }

    return FALSE;
}

//technique avancé
uint8_t nakedSingle(Damier* damier, int i, int j)
{
    return isPossibilityUnique_case(damier, i, j);
}

char* solve(Damier* damier)
{
    uint8_t d = 0;
    char* res = malloc(100*sizeof(char));
    sprintf(res, "Pas de solution");
    for(int i = 0; i<damier->line; ++i)
    {
        for(int j = 0; j<damier->column; ++j)
        {
            if(d = fullBlock(damier, i, j))
            {
                sprintf(res, "Full block sur (%d, %d): %d", i, j,d);
                return res;
            }
            else if ((d = unique(damier, i, j)))
            {
                sprintf(res, "Unique sur (%d, %d): %d", i, j, d);
                return res;
            }
            else if(d = simpleRound(damier, i, j))
            {
                sprintf(res, "Simple Round sur (%d, %d): %d", i, j, d);
                return res;
            }
            else if (d = nakedSingle(damier, i, j))
            {
                sprintf(res, "naked single sur (%d, %d): %d", i, j, d);
                return res;
            }
        }
    }
    return res;
}


