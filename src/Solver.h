#ifndef __SOLVER_H__
#define __SOLVER_H__

#include "Suguru.h"

void reduce_possibility_full(Damier* damier);
void reduce_possibility_case(Damier* damier, int i, int j);
char* solve(Damier* damier);
#endif