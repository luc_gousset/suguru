#ifndef __GAME_H__
#define __GAME_H__

#include "Suguru.h"
#include "Grid_import.h"



void initSuguru(Damier *damier, int8_t column, int8_t line, int8_t area_nb);

Case ** getNeighboor(Damier *damier, int x, int y);

Case ** create2dArray(int n, int m);

void printGrid(Case **grid, int column, int line);

void free2dArray(Case **array, int x);

uint8_t isPlayLegit(Damier * damier, int x, int y, int newValue);

int jouer_coup(Damier* damier);

void start_game(char* choice);
void suguru_loop();
#endif