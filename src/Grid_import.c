#include "Grid_import.h"

/*int8_t grille_default[6][4][2] ={{{0, EMPTY},{0,EMPTY},{1,EMPTY},{1,EMPTY}},
                                {{0, EMPTY},{0, EMPTY},{1, 3},{2,EMPTY}},
                                {{0, 5},{3, EMPTY},{2, 6},{2, 1}},
                                {{4, EMPTY},{2, 3},{2, EMPTY},{5,3}},
                                {{4, EMPTY},{2, EMPTY},{5, 1},{5, 6}},
                                {{4, EMPTY},{5, EMPTY},{5, 4},{5, EMPTY}}};*/



int8_t grille_default[2][2][2] = {{{0,EMPTY},{1,2}},
                                    {{1,4},{1,3}}};

void importGridFromArray_raw(Damier* damier, int8_t array[Grille_i][Grille_j][2])
{   
    int8_t nb_case;
    Area * area_tab = malloc(damier->area_nb*sizeof(Area));
    for(int8_t i = 0;i<damier->area_nb;i++)
    {
        area_tab[i].nb_case=0;
        area_tab[i].area_list=malloc(sizeof(Case));
        area_tab[i].area_id = i;
        
    }

    for(int i = 0; i<damier->line; i++)
    {
        for(int j = 0; j<damier->column; j++)
        {
            damier->grid[i][j].value=array[i][j][1];
            damier->grid[i][j].i = i;
            damier->grid[i][j].j = j;
            damier->grid[i][j].area = NULL;  
            damier->grid[i][j].is_alterable = TRUE;
            if (array[i][j][1])
            {
                damier->nb_case_rempli++;
                damier->grid[i][j].is_alterable = FALSE;
            }

                
            
        }
    }

    for(int i = 0; i<damier->line; i++)
    {
        for(int j = 0; j<damier->column; j++)
        {
            nb_case = area_tab[array[i][j][0]].nb_case;

            if (nb_case)
                area_tab[array[i][j][0]].area_list=realloc(area_tab[array[i][j][0]].area_list, (nb_case+1)*sizeof(Case));

            area_tab[array[i][j][0]].area_list[nb_case] = &damier->grid[i][j];

            area_tab[array[i][j][0]].nb_case++;
            
        }
    }

    for(int i = 0; i<damier->line; i++)
    {
        for(int j = 0; j<damier->column; j++)
        {
            damier->grid[i][j].area = &area_tab[array[i][j][0]]; 
            
            damier->grid[i][j].possibility = malloc(sizeof(int8_t)*(1+damier->grid[i][j].area->nb_case));
            for(int k = 0; k<1+damier->grid[i][j].area->nb_case; ++k)
            {
                damier->grid[i][j].possibility[k]=1;
            }
        }
    }

    for(int i = 0; i <damier->area_nb; i++)
    {
        if(area_tab[i].nb_case>damier->max_area_size)
            damier->max_area_size=area_tab[i].nb_case;
    }

    damier->area = area_tab;
}